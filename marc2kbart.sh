#!/bin/bash

# MARC2KBART.SH -- Download and transform records from MARC21XML to KBART format
#
# Author: Felix Hemme
# Version: v2, 2025-01-16
# License: MIT License
#
# Usage
# See README.me

# Variables
url="http://unapi.k10plus.de/?id="
target="k10plus"
schema="marcxml-solr"
mapping="marc2kbart.fix"

display_usage() {
  echo "Usage: $0 [OPTIONS]"
  echo
  echo "Beschreibung:"
  echo "  Dieses Script liest eine Datei mit PPNs ein, lädt die bibliografischen Metadaten"
  echo "  und Exemplardaten per SRU herunter und konvertiert die Daten in eine KBART-Datei."
  echo
  echo "Options:"
  echo "  -f, --file <FILE>           Specification of the input file with the PPNs."
  echo
  echo "  -m, --mapping <FIX>         Specification of the Catmandu fix file containing the mapping."
  echo "                              Default: marc2kbart.fix"
  echo
  echo "  -t, --target <TARGET>       Specification of PICA database. Available options:"
  echo "                              - k10plus (Database 1.1)"
  echo "                              - ebooks (Database 1.2)"
  echo "                              - owc-de-206 (ECONIS)"
  echo "                              Default: k10plus"
  echo
  echo "  -s, --schema <FORMAT>       Specification of format. Available options:"
  echo "                              - marcxml"
  echo "                              - marcxml-solr"
  echo "                              Default: marcxml-solr"
  echo
  echo "  -h, --help                  Display of the help menu."
}

# Use getopt to parse short and long arguments
PARSED=$(getopt -o f:m:t:s:h --long format:,output:,verbose,help -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1
fi

# Reset the options parsed by getopt back to $@
eval set -- "$PARSED"

# Parse arguments
while true; do
    case "$1" in
        -f|--file)
            file="$2"
            shift 2
            ;;
        -m|--mapping)
            mapping="$2"
            shift 2
            ;;
        -t|--target)
            target="$2"
            shift 2
            ;;
        -s|--schema)
            schema="$2"
            shift 2
            ;;
        -h|--help)
            display_usage
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option: $1"
            exit 1
            ;;
    esac
done

# Check if a file has been provided and if it exists
if [[ -z "$file" || ! -f "$file" ]]; then
    echo "Input file is missing or does not exist: ${file}"
    exit 1
fi

echo "File: $file"
echo "Mapping: $mapping"
echo "Database: $target"
echo "Format: $schema"

file_unix="${file}.unix.txt"

awk '{ sub("\r$", ""); print }' < "${file}" > "${file_unix}"

echo "Please wait... Records are being downloaded."

# Parse recordIds and download records
# Save records as records.xml
# unAPI documentation: https://wiki.k10plus.de/display/K10PLUS/UnAPI

< "${file_unix}" xargs -i curl -s "${url}${target}:ppn:{}&format=${schema}" > records.xml

# Count and display MARC record count
marc_record_count=$(grep "<controlfield tag=\"001\">" records.xml | wc -l)
echo "Number of records downloaded: "${marc_record_count}

echo "Converting records to KBART format."

# Convert records using fixes specified in fix file
# Save records as records.csv
catmandu convert MARC --type XML to CSV --fix ${mapping} --fields \
publication_title,\
print_identifier,online_identifier,date_first_issue_online,\
num_first_vol_online,num_first_issue_online,date_last_issue_online,\
num_last_vol_online,num_last_issue_online,title_url,first_author,title_id,embargo_info,coverage_depth,notes,\
publisher_name,publication_type,date_monograph_published_print,date_monograph_published_online,\
monograph_volume,monograph_edition,first_editor,parent_publication_title_id,\
preceding_publication_title_id,access_type,zdb_id --sep_char '\t' < records.xml > records.csv

# Count and display KBART record count
kbart_record_count=$(( $(wc -l < records.csv) > 1 ? $(wc -l < records.csv) - 1 : 0 ))
echo "Number of records converted: "${kbart_record_count}
