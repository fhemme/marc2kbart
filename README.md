# MARC2KBART

## Workflow to extract bibliographic records from CBS and convert to KBART

- Extract bib records from CBS (any supported database).
- Download, convert and and export as KBART compliant CSV file.
- This file can be used to ingest package titles into [GOKB](https://gokb.org/gokb-ui/).

## Usage

```bash
./marc2kbart.sh [OPTIONS]
```

The program accepts the following options:

| Option                      | Description                                                     | Default value           |
|-----------------------------|-----------------------------------------------------------------|-------------------------|
| `-f, --file <FILE>`         | Specification of the input file with the PPNs.                  |                         |
| `-m, --mapping <MAPPING>`   | Specification of the Catmandu fix file containing the mapping.  | `marc2kbart.fix`        |
| `-t, --target <DATABASE>`   | Specification of the PICA database. Available options:          | `k10plus`               |
|                             | - `k10plus` (Database 1.1)                                      |                         |
|                             | - `ebooks` (Database 1.2)                                       |                         |
| `-s, --schema <FORMAT>`     | Specification of format. Available options:                     | `marcxml-solr`          |
|                             | - `marcxml`                                                     |                         |
|                             | - `marcxml-solr`                                                |                         |
| `-h, --help`                | Display of the help menu.                                       |                         |

## How it works

This script takes record ID's (PPN's) from the K10Plus union catalogue stored in a separate file and queries the unAPI interface of a specified CBS database. The records are then being downloaded, processed and converted using Catmandu and the conversion rules specied in a fix file. Please be aware that you have to modify the mapping rules according to the local cataloging rules in your institution.

## Known issues and potential enhancements

- It is difficult to anticipate a good source field to populate the title_id for a bunch of different providers, therefore it is not included in the default fix file `marc2kbart.fix`. The value can be added manually, e.g. by copy/paste of the value in the column online_identifier, or by creating a separate fix file for a given provider and adding a mapping there.
- In case the record contains multiple ISBN's, publisher, or URL's, there is no assumption made on which is the correct one. Check the columns for multiple values delimited by a pipe character (`|` or `U+007C`).
- MARC 363 is not the appropriate element to populate coverage information. Need to wait until VZG headquarter adds Pica+ 231@ to the MARC21 export.
- The record id can be written into the first column 'identifier'. This is mainly for debugging purposes. To enable this function, add `ppn` into the Catmandu `--fields` option in the shell script. Before uploading the data into Folio the column has to be removed or the mapping rule has to be deleted in the 'marc2kbart.fix' file.
- In addition to PPN files the download method could be extended to also support queries using package ISIL or library specific Abrufzeichen. To enable this, the programm has to support SRU in addition to unAPI.